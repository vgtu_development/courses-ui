import Vue from "vue";
import VueRouter from "vue-router";
import CoursesPage from '../components/courses-page/CoursesPage.vue'
import Course from '.././components/course/Course.vue'
import CourseStageList from "../components/course/CourseStagesList";

Vue.use(VueRouter);

const routes = [
  { path: '/', component: CoursesPage },
  { path: '/course/:id', component: Course },
  { path: '/course/:id/content', component: CourseStageList }
]

export default new VueRouter({
  routes
})