# courses-ui
## **For correct operation, the application REQUIRED a started `courses-provider` application**

### Quick start:
1) start `courses-provider` application
2) in root dir: `npm install`
3) in root dir: `npm run serve` (only for dev)

### Env vars (if you need to change it):
| ENV name                                      | Default               |
| -------------                                 | -------------         |
|VUE_APP_COURSES_PROVIDER_URL                   |"http://127.0.0.1:3000"|
|VUE_APP_COURSES_PROVIDER_COURSES_PREFIX        |"/courses"             |
|VUE_APP_COURSES_PROVIDER_COURSES_LIST_PREFIX   |"/courses/list"        |
|VUE_APP_COURSES_PROVIDER_COURSES_PREFIX_CONTENT|"/content"             |

#### Project setup
```
npm install
```

#### Compiles and hot-reloads for development
```
npm run serve
```

#### Compiles and minifies for production
```
npm run build
```

#### Lints and fixes files
```
npm run lint
```

#### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
